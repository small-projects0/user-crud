package controllers

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/mini-algorithms/user-crud-project.git/database"
	"gitlab.com/mini-algorithms/user-crud-project.git/models"
	"gorm.io/gorm"
)

type User struct{}

// DESC - Full data is returned.
func (User) Index(c *fiber.Ctx) error {
	db := database.Conn.DB
	userData := []models.Users{}
	db.Model(&models.Users{}).Preload("Identification.Adresses.Contact").Find(&userData)
	return c.JSON(fiber.Map{"data": userData})
}

// DESC - User Show
func (User) Show(c *fiber.Ctx) error {

	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"message": err})
	}
	return c.JSON(fiber.Map{"data": getUser(uint(id))})
}

func getUser(id uint) models.Users {
	db := database.Conn.DB
	userData := models.Users{}
	db.Preload("Identification.Adresses.Contact").First(&userData, id)
	return userData
}

// DESC - User Delete
func (User) Delete(c *fiber.Ctx) error {
	db := database.Conn.DB
	userData := models.Users{}
	db.Delete(&userData, c.Params("id"))
	return c.JSON(fiber.Map{"message": "Success"})
}

// DESC - User Create and Update
func (User) Store(c *fiber.Ctx) error {
	var user models.Users
	var err error
	db := database.Conn.DB

	if err = c.BodyParser(&user); err != nil {
		return err
	}
	if user.ID == 0 {
		err = db.Transaction(func(tx *gorm.DB) error {
			err = db.Create(&user).Error
			if err != nil {
				return err
			}
			return nil
		})
		if err != nil {
			return c.Status(500).JSON(fiber.Map{"message": err})
		}
	} else {

		err = db.Model(&user).Omit("Identification", "Adresses", "Contact").Updates(&user).Error
		if err != nil {
			return c.Status(500).JSON(fiber.Map{"message": err})
		}

		err = db.Model(&user.Identification).Omit("Adresses", "Contact").Updates(&user.Identification).Error
		if err != nil {
			return c.Status(500).JSON(fiber.Map{"message": err})
		}
		err = db.Model(&user.Identification.Adresses).Omit("Contact", "City").Omit("IdentificationID").Save(&user.Identification.Adresses).Error
		if err != nil {
			return c.Status(500).JSON(fiber.Map{"message": err})
		}

		contact := []models.Contact{}
		//DESC - I throw the data into a common series in order not to request a request..
		for _, v := range user.Identification.Adresses {
			contact = append(contact, v.Contact...)
		}
		//DESC - I throw the requests at once.
		err = db.Model(&contact).Omit("Contact", "City").Omit("IdentificationID").Save(&contact).Error

		if err != nil {
			return c.Status(500).JSON(fiber.Map{"message": err})
		}

	}

	if err != nil {
		return c.Status(500).JSON(fiber.Map{"message": err})
	}

	user = getUser(user.ID)

	return c.JSON(fiber.Map{"message": "Success", "data": user})
}
