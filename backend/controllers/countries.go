package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mini-algorithms/user-crud-project.git/database"
	"gitlab.com/mini-algorithms/user-crud-project.git/models"
)

type Countries struct{}

func (Countries) Index(c *fiber.Ctx) error {
	db := database.Conn.DB
	countryData := []models.Country{}

	//**Select data**//
	db.Find(&countryData)

	return c.Status(200).JSON(fiber.Map{"message": "Success", "data": countryData})
}

func (Countries) GetCity(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.Conn.DB
	cityData := []models.City{}

	err := db.Find(&cityData, "country_id = ?", id).Error

	if err != nil {
		return c.Status(404).JSON(fiber.Map{"message": "Not Found"})
	}
	return c.Status(200).JSON(fiber.Map{"message": "Success", "data": cityData})
}
