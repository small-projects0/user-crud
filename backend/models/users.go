package models

import (
	"database/sql"
	"time"
)

type Users struct {
	ID               uint           `gorm:"primarykey" json:"id"`
	CreatedAt        time.Time      `json:"-"`
	UpdatedAt        time.Time      `json:"-"`
	DeletedAt        sql.NullTime   `gorm:"index"  json:"-"`
	UserName         string         `json:"username"`
	Password         string         `json:"password"`
	IdentificationID int            `json:"-"`
	Identification   Identification `json:"identification" gorm:"foreignKey:IdentificationID;onDelete:CASCADE"`
}
