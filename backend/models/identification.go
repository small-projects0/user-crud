package models

import (
	"database/sql"
	"time"
)

type Identification struct {
	ID             uint         `gorm:"primarykey" json:"id"`
	CreatedAt      time.Time    `json:"-"`
	UpdatedAt      time.Time    `json:"-"`
	DeletedAt      sql.NullTime `gorm:"index" json:"-"`
	FirstName      string       `json:"name"`
	LastName       string       `json:"surname"`
	BirthDate      string       `json:"birth_date"`
	IdentityNumber int          `json:"identity_number"`
	Adresses       []Adress     `json:"adreses" gorm:"foreignKey:IdentificationID;onDelete:CASCADE"`
}
