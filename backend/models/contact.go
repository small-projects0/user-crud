package models

import (
	"database/sql"
	"time"
)

type Contact struct {
	ID            uint         `gorm:"primarykey" json:"id"`
	CreatedAt     time.Time    `json:"-"`
	UpdatedAt     time.Time    `json:"-"`
	DeletedAt     sql.NullTime `gorm:"index" json:"-"`
	Contact       string       `json:"contact"`
	ContactTypeID int          `json:"contact_type_id"`
	AdressesID    int          `json:"-"`
}

// Contact Type Enum.
// I give Enum for Go according to the identified ids.
