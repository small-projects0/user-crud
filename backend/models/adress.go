package models

import (
	"database/sql"
	"time"
)

type Adress struct {
	ID               uint         `gorm:"primarykey" json:"id"`
	CreatedAt        time.Time    `json:"-"`
	UpdatedAt        time.Time    `json:"-"`
	DeletedAt        sql.NullTime `gorm:"index"  json:"-"`
	Detail           string       `json:"detail"`
	CityID           int          `json:"city_id"`
	CountryID        int          `json:"country_id"`
	AdressTypeID     int          `json:"addres_type_id"`
	City             City         `json:"city" gorm:"foreignKey:CityID"`
	IdentificationID int          `json:"-" gorm:"foreignKey:IdentificationID;onDelete:CASCADE"`
	Contact          []Contact    `json:"contact" gorm:"foreignKey:AdressesID;onDelete:CASCADE"`
}

// AdressTypeID Type Enum.
// I give Enum for Go according to the identified ids.
