package models

import (
	"gorm.io/gorm"
)

type Country struct {
	ID   int    `json:"id" gorm:"primaryKey"`
	Name string `json:"name"`
	City []City `json:"city,omitempty" gorm:"foreignKey:CountryID"`
}

type City struct {
	ID        int     `json:"id" gorm:"primaryKey"`
	Name      string  `json:"name"`
	CountryID int     `json:"country_id" gorm:"foreignKey:ID"`
	Country   Country `json:"country" gorm:"foreignKey:CountryID"`
}

func (City) Seed(db *gorm.DB) {
	DummyData := []Country{
		{
			Name: "Türkiye",
			City: []City{
				{
					Name:      "İstanbul",
					CountryID: 1,
				},
				{
					Name:      "Ankara",
					CountryID: 1,
				},
				{
					Name:      "İzmir",
					CountryID: 1,
				},
				{
					Name:      "Bursa",
					CountryID: 1,
				},
				{
					Name:      "Adana",
					CountryID: 1,
				},
				{
					Name:      "Denizli",
					CountryID: 1,
				},
				{
					Name:      "Samsun",
					CountryID: 1,
				},
			},
		},
		{
			Name: "Azerbaycan",
			City: []City{
				{
					Name:      "Bakü",
					CountryID: 2,
				},
				{
					Name:      "Gence",
					CountryID: 2,
				},
				{
					Name:      "Sumgayıt",
					CountryID: 2,
				},
			},
		},
	}

	db.Create(&DummyData)
}
