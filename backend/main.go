package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"gitlab.com/mini-algorithms/user-crud-project.git/database"
	"gitlab.com/mini-algorithms/user-crud-project.git/routes"
	"gitlab.com/mini-algorithms/user-crud-project.git/secret"
)

func main() {
	secret.LoadEnv("main", true)
	//DESC - Connect to database
	database.Conn.Connect()
	//DESC - Redis connection

	app := fiber.New(fiber.Config{})
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	api := app.Group("/api")

	routes.Setup(api)

	app.Listen(":8000")
}
