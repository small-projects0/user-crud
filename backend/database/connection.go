package database

import (
	"fmt"
	"time"

	"gitlab.com/mini-algorithms/user-crud-project.git/models"
	"gitlab.com/mini-algorithms/user-crud-project.git/secret"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type CstmTime time.Time

type Credentials struct {
	host    string
	port    string
	user    string
	pass    string
	sslMode string
	dBName  string
	schemas []string
}

type Database struct {
	myCredentials Credentials
	DB            *gorm.DB
}

var (
	Conn *Database
)

func (db *Database) Connect(dbname ...string) {
	if Conn != nil {
		dbCon, _ := db.DB.DB()
		dbCon.Close()
	}
	envPostEnv := secret.Env["myCredentials"].(map[string]interface{})
	db = &Database{}
	db.myCredentials = Credentials{
		host:    envPostEnv["host"].(string),
		port:    envPostEnv["port"].(string),
		user:    envPostEnv["user"].(string),
		pass:    envPostEnv["pass"].(string),
		sslMode: envPostEnv["sslMode"].(string),
		dBName:  envPostEnv["dbName"].(string),
	}
	if len(dbname) != 0 {
		db.myCredentials.dBName = dbname[0]
	}
	for _, v := range envPostEnv["schemas"].([]interface{}) {
		db.myCredentials.schemas = append(db.myCredentials.schemas, v.(string))
	}
	dsn := "host=" + db.myCredentials.host + " port=" + db.myCredentials.port + " user=" + db.myCredentials.user + " password=" + db.myCredentials.pass + " dbname=" + db.myCredentials.dBName + " sslmode=" + db.myCredentials.sslMode

	dba, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})

	if err != nil {
		panic(err)
	}
	db.DB = dba
	Conn = db
}

func (db *Database) Migrate() {
	var err error
	for {
		for _, schema := range db.myCredentials.schemas {

			Conn.DB.Exec("CREATE SCHEMA IF NOT EXISTS " + schema + " AUTHORIZATION " + Conn.myCredentials.user + ";")
			fmt.Println("Migrating schema: " + schema)
			for _, v := range migrateRelationList {
				if err := Conn.DB.SetupJoinTable(v.Model, v.Field, v.JoinTable); err != nil {
					fmt.Println(err.Error())
				}
			}
			err = Conn.DB.AutoMigrate(migrateModelList...)
			fmt.Println(err)

		}
		if err != nil {
			fmt.Println(err)
		}
		if err == nil {
			break
		}
	}
}
func (db *Database) DropSchema(schema string) {
	if err := db.DB.Exec("DROP SCHEMA IF EXISTS " + schema + " CASCADE;").Error; err != nil {
		panic(err)
	}
}

func (db *Database) Seed() {

	models.City{}.Seed(db.DB)
}
