package database

import "gitlab.com/mini-algorithms/user-crud-project.git/models"

type MigrateParams struct {
	Model     interface{}
	Field     string
	JoinTable interface{}
}

var (
	migrateRelationList = []MigrateParams{}

	migrateModelList = []interface{}{
		models.Contact{},
		models.Identification{},
		models.Users{},
		models.Adress{},
		models.Country{},
		models.City{},
	}
)
