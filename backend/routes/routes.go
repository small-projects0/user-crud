package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mini-algorithms/user-crud-project.git/controllers"
	"gitlab.com/mini-algorithms/user-crud-project.git/database"
)

func Setup(app fiber.Router) {

	//DESC - There is no need for transaction load for cities.But I wanted to get details.
	app.Get("/countries", controllers.Countries{}.Index)
	app.Get("/city/:id", controllers.Countries{}.GetCity)

	//DESC - Route was created.But the client Side process was walked for Front..
	app.Get("/get/:id<int>", controllers.User{}.Show)

	app.Post("/create", controllers.User{}.Store)

	//DESC -  Update will be made with the ids of all data.So only User'id would not be enough.
	app.Put("/update", controllers.User{}.Store)

	app.Get("/getAll", controllers.User{}.Index)
	app.Delete("/delete/:id", controllers.User{}.Delete)

	//DESC - I wanted to create a route for the migration process. It should be closed during the Production process.
	app.Get("/mig", func(c *fiber.Ctx) error {
		database.Conn.DropSchema("public")
		database.Conn.Migrate()
		database.Conn.Seed()
		return c.SendString("public dropped and created")
	})
}

// ▪ getAll: Returns all users
// ▪ get/{id}: Return the user with the desired “id”
// ▪ create: Save the given user.
// ▪ update/{id}: Update data of the user with the desired “id”
// ▪ delete/{id}: Delete the user with the desired “id”
