import React, { useEffect, useState } from "react";
import { Controller, useFieldArray, useForm } from "react-hook-form";
//**MUI Import */
import {
  Button,
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
  Grid,
  Typography,
  Slide,
  TextField,
  Card,
  CardHeader,
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  Tooltip,
  Autocomplete,
  DialogTitle,
  FormHelperText,
} from "@mui/material";
import { TransitionProps } from "@mui/material/transitions";

//**Icon Import */
import CloseIcon from "@mui/icons-material/Close";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import PasswordIcon from "@mui/icons-material/Password";
import AddIcon from "@mui/icons-material/Add";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

//**Types Import */
import { UserData } from "../../../types/userData";

//**Date Picker Import */

import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import axios from "../../../config/axios";

//**Component Import */
import UserContact from "./UserContact";
import UserAddress from "./UserAddress";

//**Yup */
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

//**React Model Slide Effect Func */
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

//**Props */
interface Props {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  editData: UserData;
  rows: UserData[];
  setRows: React.Dispatch<React.SetStateAction<UserData[]>>;
}
const userDataValidationSchema = yup.object().shape({
  id: yup.number().required(),
  username: yup.string().required(),
  password: yup.string().min(6).required(),
  identification: yup.object().shape({
    name: yup.string().required(),
    surname: yup.string().required(),
    identity_number: yup.number().required(),
    birth_date: yup.date().required(),
    adreses: yup.array().of(
      yup.object().shape({
        country_id: yup.number().min(1).required(),
        city_id: yup.number().min(1).required(),
        addres_type_id: yup.number().min(1).required(),
        contact: yup.array().of(
          yup.object().shape({
            contact: yup.string().required(),
            contact_type_id: yup.number().min(1).required(),
          })
        ),
      })
    ),
  }),
});

export default function FullScreenDialog(props: Props) {
  //**State */
  const { open, setOpen, editData, setRows, rows } = props;
  const [showPassword, setShowPassword] = useState(false);

  const defaultValues: UserData = {
    id: 0,
    username: "",
    password: "",
    identification: {
      name: "",
      surname: "",
      identity_number: 0,
      birth_date: new Date(),
      adreses: [
        {
          country_id: 0,
          city_id: 0,
          addres_type_id: 0,
          detail: "",
          contact: [
            {
              contact: "",
              contact_type_id: 0,
            },
          ],
        },
      ],
    },
  };

  useEffect(() => {
    if (Object.keys(editData).length > 0) {
      reset(editData);
    } else {
      reset(defaultValues);
    }
  }, [editData]);

  //**Hook Form */
  const {
    handleSubmit,
    control,
    reset,
    setValue,
    formState: { errors },
    getValues,
  } = useForm({
    resolver: yupResolver(userDataValidationSchema),
    defaultValues,
  });

  //**Hook form func */
  const handleClose = () => {
    setOpen(false);
  };

  const handleAdressAdd = () => {
    adresesFieldArray.append({
      city_id: 0,
      addres_type_id: 0,
      detail: "",
      country_id: 0,
      contact: [
        {
          contact: "",
          contact_type_id: 0,
        },
      ],
    });
  };

  //*Use Field Array
  const adresesFieldArray = useFieldArray({
    control,
    name: "identification.adreses",
  });

  //**Func */
  const onSubmit = (data: any) => {
    //DESC -  Error messages were given in config
    if (Object.keys(editData).length > 0) {
      //DESC -  Since there are related database tables, all relationships are required in the update process.For this, it is sent together with all Data ids.
      axios.put("/update", data).then((res) => {
        const updatedData = rows.map((row) => {
          if (row.id === res.data.id) {
            return res.data;
          }
          return row;
        });
        setRows(updatedData);
        setOpen(false);
      });
    } else {
      axios.post("/create", data).then((res) => {
        setRows((rows) => [...rows, res.data.data as UserData]);
        setOpen(false);
      });
    }
  };

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  //DESC -  Random password generate
  const handleRandomPasswordGenerate = () => {
    const randomPassword = Math.random().toString(36).slice(-8);
    setValue("password", randomPassword);
  };

  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <AppBar sx={{ position: "relative" }}>
            <Toolbar>
              <IconButton
                edge="start"
                color="inherit"
                onClick={handleClose}
                aria-label="close"
              >
                <CloseIcon />
              </IconButton>
              <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                User Created
              </Typography>
              <Button type="submit" autoFocus color="inherit">
                {Object.keys(editData).length > 0 ? "Update" : "Create"}
              </Button>
            </Toolbar>
          </AppBar>

          <Grid container spacing={2} sx={{ p: 2 }}>
            <Grid item lg={6} xs={12}>
              {/** User Information  */}
              <Card sx={{ padding: 2 }}>
                <CardHeader title="Login information" />
                <Grid container spacing={2}>
                  <Grid item xs={12} lg={12}>
                    <Controller
                      name="username"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          error={errors.username ? true : false}
                          helperText={
                            errors.username ? errors.username.message : ""
                          }
                          {...field}
                          label="User Name"
                          variant="outlined"
                          fullWidth
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} lg={12}>
                    <Controller
                      name="password"
                      control={control}
                      render={({ field }) => (
                        <FormControl fullWidth variant="outlined">
                          <InputLabel htmlFor="outlined-adornment-password">
                            Password
                          </InputLabel>
                          <OutlinedInput
                            {...field}
                            type={showPassword ? "text" : "password"}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowPassword}
                                  edge="end"
                                >
                                  {showPassword ? (
                                    <VisibilityOff />
                                  ) : (
                                    <Visibility />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            }
                            startAdornment={
                              <InputAdornment position="start">
                                <Tooltip title={"Random Password Generate"}>
                                  <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={() =>
                                      handleRandomPasswordGenerate()
                                    }
                                    edge="start"
                                  >
                                    <PasswordIcon />
                                  </IconButton>
                                </Tooltip>
                              </InputAdornment>
                            }
                            label="Password"
                          />
                        </FormControl>
                      )}
                    />
                  </Grid>
                </Grid>
              </Card>
            </Grid>
            <Grid item lg={6} xs={12}>
              {/** User Information  */}
              <Card sx={{ padding: 2 }}>
                <CardHeader title="User Information" />
                <Grid container spacing={2}>
                  <Grid item xs={12} lg={6}>
                    <Controller
                      name="identification.name"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          {...field}
                          error={errors.identification?.name ? true : false}
                          helperText={
                            errors.identification?.name
                              ? errors.identification?.name.message
                              : ""
                          }
                          label="Name"
                          variant="outlined"
                          fullWidth
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} lg={6}>
                    <Controller
                      name="identification.surname"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          {...field}
                          error={errors.identification?.surname ? true : false}
                          helperText={
                            errors.identification?.surname
                              ? errors.identification?.surname.message
                              : ""
                          }
                          label="Surname"
                          variant="outlined"
                          fullWidth
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} lg={6}>
                    <Controller
                      name="identification.identity_number"
                      control={control}
                      render={({ field }) => (
                        <FormControl fullWidth variant="outlined">
                          <InputLabel>Identity Number</InputLabel>
                          <OutlinedInput
                            {...field}
                            error={
                              errors.identification?.identity_number
                                ? true
                                : false
                            }
                            type={"text"}
                            label="Password"
                          />
                          {errors.identification?.identity_number && (
                            <FormHelperText error>
                              {errors.identification?.identity_number.message}
                            </FormHelperText>
                          )}
                        </FormControl>
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} lg={6}>
                    <Controller
                      name="identification.birth_date"
                      control={control}
                      render={({ field }) => (
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <DatePicker
                            {...field}
                            label="Bird Date"
                            renderInput={(params) => (
                              <TextField
                                error={
                                  errors.identification?.birth_date
                                    ? true
                                    : false
                                }
                                helperText={
                                  errors.identification?.birth_date
                                    ? errors.identification?.birth_date.message
                                    : ""
                                }
                                fullWidth
                                {...params}
                              />
                            )}
                          />
                        </LocalizationProvider>
                      )}
                    />
                  </Grid>
                </Grid>
              </Card>
            </Grid>
            <Grid container spacing={2} sx={{ p: 2 }}></Grid>
            {/** User Adress  */}
            {adresesFieldArray.fields.map((item, index) => {
              return (
                <>
                  <Grid item lg={12} xs={12}>
                    <Card sx={{ padding: 2 }}>
                      <Grid container spacing={2} justifyContent={"flex-end"}>
                        <Grid item xs={11}>
                          <CardHeader title="Adress Detail" />
                        </Grid>
                        <Grid item xs={1}>
                          {adresesFieldArray.fields.length - 1 === index && (
                            <IconButton onClick={() => handleAdressAdd()}>
                              <AddIcon />
                            </IconButton>
                          )}
                          {index > 0 && (
                            <IconButton
                              onClick={() => adresesFieldArray.remove(index)}
                            >
                              <DeleteForeverIcon />
                            </IconButton>
                          )}
                        </Grid>
                      </Grid>
                      {errors && (
                        <FormHelperText error>
                          {"Lütfen Tüm Alanları Doldurunuz"}
                        </FormHelperText>
                      )}
                      <UserAddress
                        getValues={getValues}
                        indexValue={index}
                        control={control}
                        errors={errors}
                      />
                      <Card>
                        {errors && (
                          <FormHelperText error>
                            {"Lütfen Tüm Alanları Doldurunuz"}
                          </FormHelperText>
                        )}
                        <UserContact
                          errors={errors}
                          parentIndex={index}
                          control={control}
                        />
                      </Card>
                    </Card>
                  </Grid>
                </>
              );
            })}
          </Grid>
        </form>
      </Dialog>
    </div>
  );
}
