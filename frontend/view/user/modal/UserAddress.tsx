import { Autocomplete, FormControlState, Grid, TextField } from "@mui/material";
import axios from "../../../config/axios";
import React, { useContext, useEffect, useState } from "react";
import { AppContext } from "../../../context/AppContext";
import { Controller } from "react-hook-form";
//**EnumData Import */
import { adresTypes } from "../../../enumData/adres_type";
import { City } from "../../../types/constData";
import { UserData } from "../../../types/userData";

interface Props {
  indexValue: number;
  control: any;
  getValues: any;
  errors: any;
}

const UserAddress = (props: Props) => {
  const { indexValue, control, getValues, errors } = props;
  const { countries } = useContext(AppContext);
  const [city, setCity] = useState<City[] | null>(null);

  //If there is a country when it is opened to edit, it brings and sets the city of that country.
  useEffect(() => {
    if (getValues("identification.adreses")[indexValue].country_id) {
      handlerGetCity(
        getValues("identification.adreses")[indexValue].country_id
      );
    }
  }, []);

  const handlerGetCity = (country_id: number) => {
    axios.get(`/city/${country_id}`).then((res) => {
      setCity(res.data.data);
    });
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} lg={6}>
        <Controller
          name={`identification.adreses.${indexValue}.country_id`}
          control={control}
          render={({ field }) => (
            <Autocomplete
              options={countries}
              onChange={(event, value, onChange) => {
                if (value) {
                  handlerGetCity(value.id);
                  field.onChange(value.id);
                }
              }}
              value={
                countries ? countries.find((c) => c.id === field.value) : null
              }
              getOptionLabel={(option) => option.name}
              renderInput={(params) => (
                <TextField {...params} label="Countries" />
              )}
            />
          )}
        />
      </Grid>
      <Grid item xs={12} lg={6}>
        <Controller
          name={`identification.adreses.${indexValue}.city_id`}
          control={control}
          render={({ field }) => (
            <Autocomplete
              onChange={(_, value) => {
                if (value) {
                  field.onChange(value.id);
                }
              }}
              options={
                !city
                  ? [
                      {
                        name: "Please choose a country...",
                        id: 0,
                        country_id: 0,
                      },
                    ]
                  : city
              }
              value={city ? city.find((c) => c.id === field.value) : null}
              getOptionLabel={(option) => option.name}
              renderInput={(params) => (
                <TextField {...params} label="Countries" />
              )}
            />
          )}
        />
      </Grid>
      <Grid item xs={12} lg={6}>
        <Controller
          name={`identification.adreses.${indexValue}.addres_type_id`}
          control={control}
          render={({ field }) => (
            <Autocomplete
              onChange={(_, value) => {
                if (value) {
                  field.onChange(value.id);
                }
              }}
              options={adresTypes}
              value={
                adresTypes ? adresTypes.find((c) => c.id === field.value) : null
              }
              getOptionLabel={(option) => option.name}
              renderInput={(params) => (
                <TextField {...params} label="Addres type " />
              )}
            />
          )}
        />
      </Grid>
    </Grid>
  );
};
export default UserAddress;
