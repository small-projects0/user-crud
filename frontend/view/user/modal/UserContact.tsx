import { Control, Controller, useFieldArray } from "react-hook-form";

//**MUI Import */
import {
  Grid,
  TextField,
  CardHeader,
  IconButton,
  Autocomplete,
} from "@mui/material";

//**Icon Import */
import AddIcon from "@mui/icons-material/Add";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

//**Types Import */
import { UserData } from "../../../types/userData";

import { contactTypes } from "../../../enumData/contact_type";

interface Props {
  control: Control<UserData, any>;
  parentIndex: number;
  errors: any;
}

const UserContact = (props: Props) => {
  const { control, parentIndex, errors } = props;

  const contactArray = useFieldArray({
    control,
    name: `identification.adreses.${parentIndex}.contact`,
  });

  //**Hook form func */

  const handleContactAdd = () => {
    contactArray.append({
      contact: "",
      contact_type_id: 0,
    });
  };
  return (
    <>
      {contactArray.fields.map((contact, index) => (
        <Grid container spacing={2} padding={2} key={contact.id}>
          <Grid
            container
            spacing={2}
            alignItems={"center"}
            justifyContent={"flex-start"}
          >
            <Grid item>
              <CardHeader title="Contact Detail" />
            </Grid>
            <Grid item>
              {contactArray.fields.length - 1 === index && (
                <IconButton onClick={() => handleContactAdd()}>
                  <AddIcon />
                </IconButton>
              )}
              {index > 0 && (
                <IconButton onClick={() => contactArray.remove(index)}>
                  <DeleteForeverIcon />
                </IconButton>
              )}
            </Grid>
          </Grid>
          <Grid item xs={12} lg={6}>
            <Controller
              name={`identification.adreses.${parentIndex}.contact.${index}.contact`}
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  label="Contact"
                  variant="outlined"
                  fullWidth
                />
              )}
            />
          </Grid>
          <Grid item xs={12} lg={6}>
            <Controller
              name={`identification.adreses.${parentIndex}.contact.${index}.contact_type_id`}
              control={control}
              render={({ field }) => (
                <Autocomplete
                  onChange={(_, value) => {
                    if (value) {
                      field.onChange(value.id);
                    }
                  }}
                  options={contactTypes}
                  value={
                    contactTypes
                      ? contactTypes.find((c) => c.id === field.value)
                      : null
                  }
                  getOptionLabel={(option) => option.name}
                  renderInput={(params) => (
                    <TextField {...params} label="Contact type " />
                  )}
                />
              )}
            />
          </Grid>
        </Grid>
      ))}
    </>
  );
};
export default UserContact;
