import { useEffect, createContext, useState } from "react";
import { Country } from "../types/constData";
import axios from "../config/axios";
interface PropsType {
  countries: Country[];
}

const AppContext = createContext<PropsType>({
  countries: [],
});

const AppProviders = ({ children }: { children: JSX.Element }) => {
  const [countries, setCountries] = useState<Country[]>([]);

  useEffect(() => {
    if (countries.length === 0) {
      axios.get("/countries").then((res) => {
        setCountries(res.data.data);
      });
    }
  }, []);

  return (
    <AppContext.Provider value={{ countries }}>{children}</AppContext.Provider>
  );
};

export { AppProviders, AppContext };
