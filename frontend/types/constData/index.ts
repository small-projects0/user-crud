type Base = {
  id: number;
  name: string;
};

//*Exporting the types
export type Country = Base;

export type City = Base & {
  country_id: number;
};
