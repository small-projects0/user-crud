type Contact = {
  contact: string;
  contact_type_id: number;
};

type Adreses = {
  city_id: number;
  country_id: number;
  addres_type_id: number;
  detail: string;
  contact: Contact[];
};

type Identification = {
  name: string;
  surname: string;
  identity_number: number;
  birth_date: Date;
  adreses: Adreses[];
};

export type UserData = {
  id: number;
  username: string;
  password: string;
  identification: Identification;
};
