//**React import */
import React from "react";
import { useState, ReactNode } from "react";

//**MUI import */
import Box from "@mui/material/Box";

//**MUI DataGrid import */
import {
  DataGrid,
  GridColDef,
  GridRenderCellParams,
  GridValueGetterParams,
} from "@mui/x-data-grid";
import UserModal from "../view/user/modal";
import { Button, Grid, IconButton } from "@mui/material";
import { UserData } from "../types/userData";
import { serverSideConfig } from "../config/axios";
import { GetServerSideProps } from "next/types";
import PersonAddAlt1Icon from "@mui/icons-material/PersonAddAlt1";
import PersonRemoveIcon from "@mui/icons-material/PersonRemove";

import axios from "../config/axios";

type Props = {
  data: UserData[];
};

const Home = (props: Props) => {
  const columns: GridColDef[] = [
    { field: "id", headerName: "ID" },
    {
      field: "username",
      headerName: "Username",
    },
    {
      field: "identification.name",
      headerName: "Name",
      valueGetter: (params: GridValueGetterParams) =>
        `${params.row.identification.name}`,
    },
    {
      field: "identification.surname",
      headerName: "Surname",
      valueGetter: (params: GridValueGetterParams) =>
        `${params.row.identification.surname}`,
    },
    {
      field: "identification.birth_date",
      headerName: "Doğum Tarihi",
      valueGetter: (params: GridValueGetterParams) =>
        `${params.row.identification.birth_date}`,
    },
    {
      field: "actions",
      headerName: "Actions",
      sortable: false,
      width: 160,

      renderCell: (
        params: GridRenderCellParams<string | string[]>
      ): ReactNode => {
        return (
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <IconButton onClick={() => openEditModal(params.row)}>
              <PersonAddAlt1Icon />
            </IconButton>
            <IconButton onClick={() => handlerRemoveUser(params.row.id)}>
              <PersonRemoveIcon />
            </IconButton>
          </Box>
        );
      },
    },
  ];

  const [open, setOpen] = useState<boolean>(false);
  const [rows, setRows] = useState<UserData[]>(props.data);
  const [editData, setEditData] = useState<UserData>({} as UserData);

  //**Func */
  const openEditModal = (data: UserData) => {
    setEditData(data);
    setOpen(true);
  };

  const handlerRemoveUser = (id: number) => {
    axios.delete(`/delete/${id}`).then((res) => {
      const newRows = rows.filter((row) => row.id !== id);
      setRows(newRows);
    });
  };

  return (
    <>
      <Grid container spacing={3} justifyContent={"end"}>
        <Grid item xs={2}>
          <Button
            fullWidth
            variant="outlined"
            onClick={() => {
              setOpen(true);
              setEditData({} as UserData);
            }}
          >
            Add New User
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Box sx={{ height: 400, width: "100%" }}>
            <DataGrid
              rows={rows}
              columns={columns}
              pageSize={5}
              rowsPerPageOptions={[5]}
              disableSelectionOnClick
              experimentalFeatures={{ newEditingApi: true }}
            />
          </Box>
        </Grid>
      </Grid>
      <UserModal
        rows={rows}
        setRows={setRows}
        editData={editData}
        open={open}
        setOpen={setOpen}
      />
    </>
  );
};
export default Home;

// DESC - get serverSide props
export const getServerSideProps: GetServerSideProps = async (context) => {
  const config = serverSideConfig(context);
  try {
    const request = await axios.get(`/getAll`, config);
    const data = request.data.data;
    return {
      props: {
        data,
      },
    };
  } catch (error) {
    return {
      redirect: {
        destination: "/500/",
      },
      props: {},
    };
  }
};
