import { Base } from "./base";

type ContactType = Base;

export const contactTypes: ContactType[] = [
  {
    id: 1,
    name: "Phone",
  },
  {
    id: 2,
    name: "Email",
  },
  {
    id: 3,
    name: "Other",
  },
];
