import { Base } from "./base";

type adresType = Base;

export const adresTypes: adresType[] = [
  {
    id: 1,
    name: "Home",
  },
  {
    id: 2,
    name: "Work",
  },
  {
    id: 3,
    name: "Other",
  },
];
