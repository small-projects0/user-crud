import axios, { AxiosRequestConfig } from "axios";

//**NextJS Import */

import { GetServerSidePropsContext, PreviewData } from "next/types";
import { ParsedUrlQuery } from "querystring";

//DESC - Axios default settings

const config: AxiosRequestConfig = {
  baseURL: "http://127.0.0.1:8000/api",
};

const instance = axios.create(config);

export const serverSideConfig = (
  context: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
): AxiosRequestConfig => {
  return {
    ...config,
  };
};

instance.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default instance;
