Summary
Your project is an API written using the Fiber library. The purpose of the project is to manage user data in the database.
Project PORT : 8000
Endpoints
GET api/countries
Lists countries.

GET api/city/:id
Lists cities according to the given id value.

GET api/get/:id
Gets user data according to the given id value.

POST api/create
Creates a new user.

PUT api/update
Updates an existing user.

GET api/getAll
Gets all user data.

DELETE api/delete/:id
Deletes user data according to the given id value.

GET api/mig
Resets and recreates the database. It is recommended to close this endpoint in the production environment.

 



Frontend File Doc.



config: Global libraries like Axios have their configs here.
context: The context structure for data to be used everywhere (we kept the countries in the context in our project).
enumDAta: We saved the address type and contact type as enum data in the frontend. Since these data should not increase, we did not add them to the database. However, we kept them as "id" in the database.
pages: Only index page has coding.
types: Types are determined.
view: Components are written.


controller:

user : 
Named User is defined and it contains the following methods:

Index: Returns all user data.
Show: Returns user data according to the given id value.
Delete: Deletes user data according to the given id value.
Store: Creates a new user or updates an existing user.

A function called getUser is also defined. This function returns user data according to the given id value.

countries:
This code block is a part of the Controllers package and it defines a class named Countries. This class has two methods:

Index: Returns all country data.
GetCity: Returns city data according to the given country id value.

The Index method returns all the country data by calling the Find method on the database connection object. The GetCity method returns city data according to the given country id value by calling the Find method on the database connection object with a filter condition. If there is an error executing the query, it returns a status code of 404 with a message of "Not Found". Otherwise, it returns a status code of 200 with the data.

models:
A package for storing database models
route:
route
secret:
An env library was not used for the project. Env was made with a json file.
database/migrate:
Where database connection, migrate, seed, and other database operations are performed
